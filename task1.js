// 1/ read jsonfile
const fs = require("fs");
const data = fs.readFileSync("./input2.json", "utf-8");
// 2/reverse Array

const nameList = JSON.parse(data);

const reverseNameList = nameList.names.reverse();

// 3/random character with gmail.com
const generateRandomChar = () => {
  const length = 5;
  const randomChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  let result = "";
  for (i = 0; i < length; i++) {
    result += randomChar.charAt(Math.floor(Math.random() * randomChar.length));
  }
  return result + "gmail.com";
};

// 4/adding 2 with 3

const finalResult = reverseNameList.map((list) => list + generateRandomChar());

// creatting new array and passing finalResult

const output = {
  emails: finalResult,
};

// create new file
fs.writeFileSync(
  "./output2.json",
  JSON.stringify(output),
  "utf-8",
  (error, data) => {
    data && console.log(data);
    error && console.log(error);
  }
);
